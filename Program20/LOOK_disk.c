#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define DISK_SIZE 100

void sort(int* arr, int size) {
    int key, i, j;
    for(j = 1; j < size; j++) {
        key = arr[j];
        i = j-1;
        while(i >= 0 && key < arr[i]) {
            arr[i+1] = arr[i];
            i--;
        } 
        arr[i+1] = key;
    }
}

void LOOK(int* requests, int head, int size) {
    int init_head = head;
    int seek_count = 0;
    int l = 0, r = 0, s = 0;
    int distance, cur_track;
    int* left = malloc(sizeof(int[size]));
    int* right = malloc(sizeof(int[size]));
    int* seekSequence = malloc(sizeof(int[2*size]));

    for(int i = 0; i < size; i++) {
        if(requests[i] < head) left[l++] = requests[i];
        else right[r++] = requests[i];
    }
    sort(left, l);
    sort(right, r);

    for(int i = 0; i < r; i++) {
        cur_track = right[i];
        seekSequence[s++] = cur_track;
        distance = abs(cur_track - head);
        seek_count += distance;
        head = cur_track;
    }

    for(int i = l-1; i >= 0; i--) {
        cur_track = left[i];
        seekSequence[s++] = cur_track;
        distance = abs(cur_track - head);
        seek_count += distance;
        head = cur_track;
    }

    printf("Seek Operations: %d\n", seek_count);
    printf("Seek Sequences: \n");
    printf("%d ->", init_head);
    for(int i = 0; i < s-1; i++) printf("%d -> ", seekSequence[i]);
    printf("%d\n", seekSequence[s-1]);
}

int main() {
    time_t t;
    srand((unsigned) time(&t));
    printf("Randomly generating the request sequence and the inital head position... \n");
    int head = rand() % 100;
    int requests[15];
    for(int i = 0; i < 15; i++) {
        requests[i] = rand() % 100;
    }
    LOOK(requests, head, 15);
    return 0;
}
