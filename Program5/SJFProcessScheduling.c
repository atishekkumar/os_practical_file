#include <pthread.h>
#include<stdio.h>
#include<stdlib.h>

struct PCB {
    int pid;
    int burstTime;
    int arrivalTime;
};

void printQueue(struct PCB* processes, int size) {
    printf("-----------------------------------------------\n");
    printf("|  Processes  |  Burst Time  |  Arrival Time  |\n");
    printf("-----------------------------------------------\n");
    for(int i = 0; i < size; i++) {
        printf("|     %d      |      %d      |       %d       |\n", processes[i].pid, processes[i].burstTime, processes[i].arrivalTime);
        printf("-----------------------------------------------\n");
        }
}

void arrangeArrivalTime(struct PCB* processes, int size) {
    //arrange the processes according to their arrival times
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size - i - 1; j++){
            if(processes[j].arrivalTime > processes[j+1].arrivalTime) {        
                struct PCB temp = processes[j];
                processes[j] = processes[j+1];
                processes[j+1] = temp;
            } 
        }
    }
}

void arrangeBurstTime(struct PCB* processes, int size) {
    // arrange the processes that have the same arrival time in order of increasing burst times
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size-i-1; j++){
            if(processes[j].arrivalTime == processes[j+1].arrivalTime) {
                if (processes[j].burstTime > processes[j+1].burstTime) {
                    struct PCB temp = processes[j];
                    processes[j] = processes[j+1];
                    processes[j+1] = temp;
                }
            }
        }
    }
}

void arrange(struct PCB* processes, int size) {
    arrangeArrivalTime(processes, size);
    arrangeBurstTime(processes, size);
}

void printStats(struct PCB* processes,int stats[][3], int size){
    int totalWaitingTime = 0, totalTurnaroundTime = 0;
    printf("-----------------------------------------------------------------------------------\n");
    printf("|  Processes  |  Burst Time  |  Arrival Time  |  Waiting time  |  Turnaround Time  |\n");
    printf("------------------------------------------------------------------------------------\n");
    for(int i = 0; i < size; i++) {
        totalWaitingTime += stats[i][2];
        totalTurnaroundTime += stats[i][1];
        printf("|     %d      |      %d      |       %d       |         %d        |       %d       |\n", processes[i].pid, processes[i].burstTime, processes[i].arrivalTime, stats[i][2], stats[i][1]);
        printf("------------------------------------------------------------------------------------\n");
    }
    printf("Average waiting time = %.2f \n", (float)totalWaitingTime /(float)size);
    printf("Average turnaround time = %.2f \n", (float)totalTurnaroundTime /(float)size);
}

void Stats(struct PCB* processes, int size, int stats[][3]){
    stats[0][0] = processes[0].burstTime + processes[0].arrivalTime;
    stats[0][1] = stats[0][0] - processes[0].arrivalTime;
    stats[0][2] = stats[0][1] - processes[0].burstTime;
    for(int i = 1; i < size; i++) {
        stats[i][0] = stats[i-1][0] + processes[i].burstTime;
        stats[i][1] = stats[i][0] - processes[i].arrivalTime;
        stats[i][2] = stats[i][1] - processes[i].burstTime;
    }
}


void SJF_Scheduling(struct PCB* processes, int size) {
    printQueue(processes, size);
    arrange(processes, size);
    int (*stats)[size] = malloc(sizeof(int[size][3]));
    Stats(processes, size, stats);
    printStats(processes, stats, size);
}

int main() {
    srand(0);
    struct PCB processes[10];
    printf("Generating random processes... \n");
    for(int i = 0 ; i < 10; i++){
        processes[i].pid = i+1;
        processes[i].burstTime = rand() % 100;
        processes[i].arrivalTime = rand() % 50;
    }
    int size = sizeof(processes) / sizeof(struct PCB);
    SJF_Scheduling(processes, size);
    return 0;
}
