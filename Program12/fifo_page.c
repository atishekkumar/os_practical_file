#include"./queue.h"
#include <stdio.h>
#include<time.h>

int fifo_page(int pages[], int pageSize, int capacity) {
    int pageFaults = 0;
    printf("Initialising the page Queue... \n");
    initQ();

    for(int i = 0; i < pageSize; i++) {
        if(Queue.size < capacity) {
            if(!Qfind(pages[i])) {
                Qinsert(pages[i]);
                printf("Page %d not found. Inserting in the queue \n", pages[i]);
                Qprint();
                pageFaults++;
            } else {
                printf("Page %d found!\n", pages[i]);
            }
        } else {
            if(!Qfind(pages[i])) {
                printf("Page %d not found. Removing %d from the front and inserting %d in the queue \n", pages[i], Qremove(), pages[i]);
                Qinsert(pages[i]);
                Qprint();
                pageFaults++;
            } else {
                printf("Page %d found!\n", pages[i]);
            }
        }
    }
    return pageFaults;
}

int main() {
    time_t t;
    srand((unsigned) time(&t));
    printf("Randomly genrating the page sequence:\t");
    int pages[15];
    for(int i = 0; i < 15; i++ ) {
        pages[i] = rand() % 6;
        printf("%d  ",pages[i]);
    } printf("\n");
    int capacity = 4;
    printf("Page Faults: %d\n",fifo_page(pages, 15, capacity));
    return 0;
}
