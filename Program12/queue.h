#include<stddef.h>
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct node {
    int index;
    struct node* next;
};

struct queue {
    struct node* head;
    struct node* tail;
    int size;
} Queue;

void initQ(){
    Queue.head = Queue.tail = NULL;
    Queue.size = 0;
}

void Qinsert(int val) {
    struct node* newNode = (struct node *)malloc(sizeof(struct node));
    newNode->index = val;
    newNode->next = NULL;
    if(!Queue.head) {
        //queue is empty
        Queue.head = newNode;
    } else {
        Queue.tail->next = newNode;
    }
    Queue.tail = newNode;
    Queue.size++;
    return;
}

int Qremove() {
    if(Queue.size == 0) return -1;
    struct node* temp = Queue.head;
    Queue.head = Queue.head->next;
    Queue.size--;
    if(Queue.size == 0) Queue.tail = NULL;
    int ret = temp->index;
    free(temp);
    return ret;
}

bool Qfind(int val) {
    struct node* cur = Queue.head;
    while(cur) {
        if(cur->index == val) return true;
        cur = cur->next;
    }
    return false;
}

void Qprint() {
    struct node* cur = Queue.head;
    printf("Queue size: %d \nQueue:\t", Queue.size);
    while(cur) {
        printf("%d\t", cur->index);
        cur = cur->next;
    } printf("\n");
}
