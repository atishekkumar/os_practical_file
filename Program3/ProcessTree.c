#include <sys/wait.h>
#include <time.h>
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<signal.h>

#define N 2
#define PATH "/home/atishek/Documents/College/Sem4/OS_practical_file/Program3/ProcessTree"

void printer(int depth) {
    if(depth != 0){
        for(int i = 3; i < depth * 10; i++) {
            printf(" ");
        }
    }
    printf("-> Process_id:%d, depth:%d\n", getpid() ,depth);
}

int main(int argc, char** argv) {
    if( argc != 2) {
        fprintf(stderr, "Usage: ./ProcessTree <depth> \n");
        return 0;
    }
    int depth = atoi(argv[1]);
    time_t t;
    srand((unsigned) time(&t));
    printer(depth);
    if(depth <= N) {
        //forking child processes
        depth++;
        char exec_Arg[20];
        snprintf(exec_Arg, sizeof(exec_Arg), "%d", depth);
        int num_child = rand() % 5 + 1;
        pid_t* child = malloc(sizeof(pid_t[num_child]));
        for(int i = 0; i < num_child; i++) {
            child[i] = fork();
            if(child[i] == 0)
                execlp(PATH, "ProcessTree", exec_Arg, NULL);
            waitpid(child[i], NULL, 0);
        }
    }
    return 0;
}
