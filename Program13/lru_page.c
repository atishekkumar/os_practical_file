#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define MAX 1000

int lru_replacement(int mem[][2], int size) {
    int rep = 0;
    int lru = MAX; 
    for(int i = 0 ; i < size; i++) {
        if(mem[i][1] < lru) {
            lru = mem[i][1];
            rep = i;
        }
    }
    return rep;
}

int find(int mem[][2], int val, int size) {
    for(int i = 0; i < size; i++) {
        if(mem[i][1] == val) return i;
    }
    return -1;
}

int lru_page(int* pages, int pageSize, int capacity) {
    int (*mem)[capacity] = malloc(sizeof(int[capacity][2]));
    int size = 0;
    int pageFaults = 0;

    for(int i = 0; i < pageSize; i++) {
        if(size < capacity) {
            int test = find(mem, pages[i], capacity);
            if( test == -1) {
                mem[size][0] = pages[i];
                mem[size][1] = i;
                pageFaults++;
            } else mem[test][1] = i;
        } else {
            int test = find(mem, pages[i], capacity);
            if(test == -1) {
                int replacement = lru_replacement(mem, capacity);
                mem[replacement][0] = pages[i];
                mem[replacement][1] = i;
                pageFaults++;
            } else mem[test][1] = i;
        } 
    }
    return pageFaults;
}

int main() {
    time_t t;
    srand((unsigned) time(&t));
    printf("Randomly generating the page sequence: \t");
    int pages[15];
    for(int i = 0; i < 15; i++ ) {
        pages[i] = rand() % 5;
        printf("%d  ",pages[i]);
    } printf("\n");
    int capacity = 4;
    printf("Page Faults: %d\n",lru_page(pages, 15, capacity));
    return 0;
}
