//child process
//program to be called from the parent process

#include "unistd.h"
#include "stdio.h"

int main() {
    printf("Child process started at: %d \n", getpid());
    printf("Processing..\n");
    sleep(1);
    printf("Processing done\n");
    return 0;
}