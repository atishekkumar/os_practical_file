// Program to create and terminate a process using fork, wait, signal and exit system calls

#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>

//change with your build path
#define PATH "/home/atishek/Documents/College/Sem4/OS_practical_file/Program2/Child"

//signal handler
void killingTheProcess(int sig){
    printf("SIGTERM caught. signal: %d\n", sig);
}

int main() {
    printf("Parent Process started at pid: %d\n", getpid());

    //providing SIGTERM with a handler
    signal(SIGTERM, killingTheProcess);

    printf("Forking a child process \n");
    pid_t pid;
    //creating a child process
    pid = fork();

    //Error creating process
    if(pid < 0) {
        fprintf(stderr, "Child process could not start");
    }
    //child process created successfully
    else if(pid == 0) {
        execlp(PATH, NULL);
    }
    //parent process
    else {
        //waiting form the child process
        wait(NULL);
        printf("Child Process Completed \n");
    }

    printf("Killing the parent process by giving SIGTERM signal \n");
    //killing the process
    kill(getpid(), SIGTERM);
    return 0;
}
