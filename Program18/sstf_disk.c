#include <stdbool.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define MAX 1000

struct read {
    int distance;
    bool accessed;
};

struct read* Read(int size) {
    struct read* p = malloc(sizeof(struct read[size]));
    for(int i = 0; i < size; i++) {
        p[i].distance = 0;
        p[i].accessed = false;
    }
    return p;
}

void calculateDiff(int* requests, int head, struct read* diff, int size) {
    for(int i = 0; i < size; i++) {
        diff[i].distance = abs(requests[i]- head);
    }
}

int findMin(struct read* diff, int size) {
    int index = -1, min = MAX;
    for(int i = 0; i < size; i++) {
        if(!diff[i].accessed && min > diff[i].distance) {
            min = diff[i].distance;
            index = i;
        }
    }
    return index;
}

void SSTF(int* requests, int head, int size) {
    struct read* diff = Read(size);
    int seek_count = 0;
    int* seekSequence = malloc(sizeof(int[size+1]));
    for(int i = 0; i < size; i++) {
        seekSequence[i] = head;
        calculateDiff(requests, head,  diff , size);
        int index = findMin(diff ,size);
        diff[index].accessed = true;
        seek_count += diff[index].distance;
        head = requests[index];
    }
    seekSequence[size] = head;
    printf("Seek Operations: %d\n", seek_count);
    printf("Seek Sequences: \n");
    for(int i = 0; i < size; i++) printf("%d -> ", seekSequence[i]);
    printf("%d\n", seekSequence[size]);
}

int main() {
    time_t t;
    srand((unsigned) time(&t));
    printf("Randomly generating the request sequence and the inital head position... \n");
    int head = rand() % 100;
    int requests[15];
    for(int i = 0; i < 15; i++) {
        requests[i] = rand() % 100;
    }
    SSTF(requests, head, 15);
    return 0;
}
