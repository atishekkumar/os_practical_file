#include<stdio.h>
#include<stdlib.h>
#include <sys/types.h>

struct PCB {
    int pid;
    int burstTime;
};

int* waitingTime(struct PCB* processes, int size){
    //calculates the waiting time for a process
    int *wait = malloc(sizeof(int) * size);
    wait[0] = 0; //waiting time for the first process is 0
    for(int i = 1; i < size; i++){
        wait[i] = processes[i-1].burstTime + wait[i-1];
    }
    return wait;
}

int* turnAroundTime(struct PCB* processes, int *wait, int size){
    int* turnaround = malloc(sizeof(int) * size);
    for(int i = 0 ; i < size; i++) {
        turnaround[i] = processes[i].burstTime + wait[i];
    }
    return turnaround;
}

void printStats(struct PCB* processes, int* wait, int* turnaround, int size){
    int totalWaitingTime = 0, totalTurnaroundTime = 0;
    printf("-------------------------------------------------------------------\n");
    printf("|  Processes  |  Burst Time  |  Waiting time  |  Turnaround Time  |\n");
    printf("-------------------------------------------------------------------\n");
    for(int i = 0; i < size; i++) {
        totalWaitingTime += wait[i];
        totalTurnaroundTime += turnaround[i];
        printf("|     %d      |      %d      |       %d       |         %d        |\n", processes[i].pid, processes[i].burstTime, wait[i], turnaround[i]);
        printf("-------------------------------------------------------------------\n");
    }
    printf("Average waiting time = %.2f \n", (float)totalWaitingTime /(float)size);
    printf("Average turnaround time = %.2f \n", (float)totalTurnaroundTime /(float)size);
}

void FCFS_Scheduling(struct PCB* processes, int size) {
    int* wait = waitingTime(processes, size);
    int* turnaround = turnAroundTime(processes, wait, size);
    printStats(processes, wait, turnaround, size);
    free(wait);
    free(turnaround);
    return;
}

int main(){
    srand(0);
    struct PCB processes[10];
    printf("Generating random processes...\n");
    for(int i = 0; i < 10; i++) {
        processes[i].pid = i+1;
        processes[i].burstTime = rand() % 100;
    }
    int size = sizeof(processes) / sizeof(struct PCB);
    FCFS_Scheduling(processes, size);
    return 0;
}
