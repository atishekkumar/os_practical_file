#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int FCFS(int* requests, int head, int size) {
    int seek_count = 0;
    int distance, cur_track;

    for(int i = 0; i < size; i++) {
        cur_track = requests[i];
        distance = abs(cur_track - head);
        seek_count += distance;
        head = cur_track;
    }
    return seek_count;
}

int main() {
    int i;
    time_t t;
    srand((unsigned) time(&t));
    printf("Randomly generating the request sequence and the inital head position... \n");
    int head = rand() % 100;
    int requests[15];
    for(int i = 0; i < 15; i++) {
        requests[i] = rand() % 100;
    }
    printf("Seek Operations: %d\n", FCFS(requests, head, 15));
    printf("Seek Sequence:\n");
    printf("%d -> ", head);
    for(i = 0; i < 14; i++) printf("%d -> ", requests[i]);
    printf("%d\n", requests[i]);
    return 0;
}
