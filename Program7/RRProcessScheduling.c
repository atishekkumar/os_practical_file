#include<stdio.h>
#include<stdlib.h>

struct PCB {
    int pid;
    int burstTime;
};

int* waitingTime(struct PCB* processes, int size, int timeQuantum) {
    int* copyBurstTime = malloc(sizeof(int) * size);
    int* wait = malloc(sizeof(int) * size);
    for(int i = 0; i < size; i++) copyBurstTime[i] = processes[i].burstTime;
    int time = 0;

    while(1) {
        int finished = 1;
        for(int i = 0; i < size; i++) {
            if(copyBurstTime[i] > 0) {
                finished = 0;
                time += copyBurstTime[i] > timeQuantum ? timeQuantum : copyBurstTime[i];
                copyBurstTime[i] -= copyBurstTime[i] > timeQuantum ? timeQuantum : copyBurstTime[i];
                if(!copyBurstTime[i]) {
                    wait[i] = time - processes[i].burstTime;
                }
            }
        }
        if(finished) break;
    }
    free(copyBurstTime);
    return wait;
}

int* turnAroundTime(struct PCB* processes, int size, int* wait) {
    int* turnaround = malloc(sizeof(int) * size);
    for(int i = 0; i < size; i++) {
        turnaround[i] = processes[i].burstTime + wait[i];
    }
    return turnaround;
}

void printStats(struct PCB* processes, int* wait, int* turnaround, int size, int timeQuantum){
    int totalWaitingTime = 0, totalTurnaroundTime = 0;
    printf("Time Quantum = %d\n", timeQuantum);
    printf("-------------------------------------------------------------------\n");
    printf("|  Processes  |  Burst Time  |  Waiting time  |  Turnaround Time  |\n");
    printf("-------------------------------------------------------------------\n");
    for(int i = 0; i < size; i++) {
        totalWaitingTime += wait[i];
        totalTurnaroundTime += turnaround[i];
        printf("|     %d      |      %d      |       %d       |         %d        |\n", processes[i].pid, processes[i].burstTime, wait[i], turnaround[i]);
        printf("-------------------------------------------------------------------\n");
    }
    printf("Average waiting time = %.2f \n", (float)totalWaitingTime /(float)size);
    printf("Average turnaround time = %.2f \n", (float)totalTurnaroundTime /(float)size);
}

void RR_Scheduling(struct PCB* processes, int size, int timeQuantum) {
    int* wait = waitingTime(processes, size, timeQuantum);
    int* turnaround = turnAroundTime(processes, size, wait);
    printStats(processes, wait, turnaround, size, timeQuantum);
    free(wait);
    free(turnaround);
    return;
}

int main() {
    srand(0);
    struct PCB processes[10];
    for(int i = 0; i < 10; i++){
        processes[i].pid = i+1;
        processes[i].burstTime = rand() % 100;
    }
    int size = 10;
    int timeQuantum = 50;
    RR_Scheduling(processes, size, timeQuantum);
    return 0;
}
