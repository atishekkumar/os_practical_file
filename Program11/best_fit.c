#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>
#include<string.h>
#include <time.h>

int* bestFit(int memoryBlock[], int memBlockSize, int processBlock[], int proBlockSize){
    int *allocated = malloc(memBlockSize * sizeof(int));

    memset(allocated, -1, memBlockSize * sizeof(int));

    for(int i = 0; i < proBlockSize; i++) {
        //find the best fit for the process block
        int fit = -1;
        for(int j = 0; j < memBlockSize; j++) {
            if(memoryBlock[j] >= processBlock[i]) {
                fit = ((fit == -1) || (fit != -1 && memoryBlock[fit] > memoryBlock[j]))? j: fit;
            }
        }
        //best block found allocate it
        if(fit != -1) {
            allocated[i] = fit;
            memoryBlock[fit] -= processBlock[i];
        }
    }
    return allocated;
}

int main() {
    time_t t;
    srand((unsigned) time(&t));

    int memoryBlock[10], processBlock[10];
    int memBlockSize = 10, proBlockSize = 10;

    printf("Randomly generating memory and process blocks \n");

    for(int i = 0; i < 10; i++) {
        memoryBlock[i] = rand() % 100;
        processBlock[i] = rand() % 100;
    }

    printf("Memory block:\t");
    for(int i = 0 ; i < 10; i++) printf("%d\t", memoryBlock[i]);
    printf("\n");

    printf("Process block:\t");
    for(int i = 0 ; i < 10; i++) printf("%d\t", processBlock[i]);
    printf("\n");

    int* allocated = bestFit(memoryBlock, memBlockSize, processBlock, proBlockSize);

    printf("Allocations:\t");
    for(int i = 0 ; i < 10; i++) printf("%d\t", allocated[i]+1);
    printf("\nZero(0) means couldn't allocate \n");

    free(allocated);
    return 0;
}
