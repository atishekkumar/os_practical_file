#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include<stdbool.h>

bool find_update(int x, int* mem, bool* second_chance, int capacity) {
    for(int i = 0; i < capacity; i++) {
        if(mem[i] == x) {
            second_chance[i] = true;
            return true;
        }
    }
    return false;
}

int replace_update(int x, int* mem, bool* second_chance, bool* modify, int capacity, int pointer) {
    int flag = 0;
    //not recently used and not modified
    while(true) {
        if(!second_chance[pointer] && !modify[pointer]) {
            mem[pointer] = x;
            flag = 1;
            break;
        }
        second_chance[pointer] = false;
        pointer = (pointer+1) % capacity;
    }
    //not recently used but modified
    if(flag == 0) {
        while(true) {
            if(!second_chance[pointer] && modify[pointer]) {
                mem[pointer] = x;
                flag = 1;
                break;
            }
            second_chance[pointer] = false;
            pointer = (pointer+1) % capacity;
        }
    }
    //recently used but not modified
    if(flag == 0) {
        while(true) {
            if(second_chance[pointer] && !modify[pointer]) {
                mem[pointer] = x;
                flag = 1;
                break;
            }
            second_chance[pointer] = false;
            pointer = (pointer+1) % capacity;
        }
    }
    //recently used and modified
    if(flag == 0) {
        while(true) {
            if(second_chance[pointer] && modify[pointer]) {
                mem[pointer] = x;
                flag = 1;
                break;
            }
            second_chance[pointer] = false;
            pointer = (pointer+1) % capacity;
        }
    }
    return (pointer + 1) % capacity;
}

int second_chance_page(int* pages, int pageSize, int capacity) {
    int pointer = 0;
    int* mem = malloc(sizeof(int[capacity]));
    bool* second_chance = malloc(sizeof(bool[capacity]));
    bool* modify_bit = malloc(sizeof(bool[capacity]));
    int pageFaults = 0;
    memset(mem, -1, sizeof(int[capacity]));

    for(int i = 0; i < pageSize; i++) {
        if(!find_update(pages[i], mem, second_chance, capacity)) {
            pointer = replace_update(pages[i], mem, second_chance,modify_bit, capacity, pointer);
            pageFaults++;
        }
    }
    return pageFaults;
}

int main() {
    time_t t;
    srand((unsigned) time(&t));
    printf("Randomly generating the page sequence: \t");
    int pages[15];
    for(int i = 0; i < 15; i++ ) {
        pages[i] = rand() % 5;
        printf("%d  ",pages[i]);
    } printf("\n");
    int capacity = 4;
    printf("Page Faults: %d\n", second_chance_page(pages, 15, capacity));
    return 0;
}
