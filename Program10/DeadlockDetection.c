#include<stdio.h>
#include<stdlib.h>

int main() {
    int n = 3;
    int r = 3;

    int max[3][3] = {
        {3, 6, 8},
        {4, 3, 3},
        {3, 4, 4}
    };
    
    int alloc[3][3] = {
        {3, 3, 3},
        {2, 0, 3},
        {1, 2, 4}
    };

    int avail[3] = {1, 2, 0};

    int i,j;
    printf("Process\t Allocation\t Max\t Available\t");
    for(i = 0; i < n; i++) {
        printf("\nP%d\t   ",i+1);
        for(j = 0; j < r; j++) {
            printf("%d ",alloc[i][j]);
        }
        printf("\t");
        for(j = 0; j < r; j++){
            printf("%d ",max[i][j]);
        }
        printf("\t");
        if(i==0) {
            for(j = 0;j < r; j++)
            printf("%d ",avail[j]);
        }
    }

    int finish[100],need[100][100],flag=1,k;
    int dead[100];

    for(i=0;i<n;i++) {
        finish[i]=0;
    }
    for(i=0;i<n;i++) {
        for(j=0;j<r;j++) {
            need[i][j]=max[i][j]-alloc[i][j];
        }
    }
    while(flag) {
        flag=0;
        for(i=0;i<n;i++) {
            int c=0;
            for(j=0;j<r;j++){
                if((finish[i]==0)&&(need[i][j]<=avail[j])){
                    c++;
                    if(c==r) {
                        for(k=0;k<r;k++) {
                            avail[k]+=alloc[i][j];
                            finish[i]=1;
                            flag=1;
                        }
                        if(finish[i]==1){
                          i=n;
                        }
                    }
                }
            }
        }
    }
    j=0;
    flag=0;
    for(i=0;i<n;i++){
        if(finish[i]==0){
            dead[j]=i;
            j++;
            flag=1;
        }
    }
    if(flag==1){
        printf("\n\nSystem is in Deadlock and the Deadlock process are\n");
        for(i=0;i<n;i++) {
            printf("P%d\t",dead[i]);
        }
    }
    else{
        printf("\nNo Deadlock Occur");
    }
    printf("\n");
return 0;
}
